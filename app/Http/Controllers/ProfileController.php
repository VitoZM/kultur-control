<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function getProfile(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $profile = Profile::all()->where("userId",$userId)->first();
            return response()->json($profile, 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $profile = Profile::all()->where("userId",$userId)->first();
            $user = User::all()->where("userId",$userId)->first();
            $profile->name = $request->name;
            $profile->lastName = $request->lastName;
            //DB::delete('delete from model_has_roles where model_id = ?',[$userId]);//eliminar rol de usuario
            //$user->assignRole($request->job);//asignar nuevo rol al usuario
            $profile->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }
}
