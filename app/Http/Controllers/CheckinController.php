<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Checkin;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CheckinController extends Controller
{
    public static function getStatus($userId)
    {
        $now = Carbon::now()->setTimeZone('America/La_Paz');
        $checkin = Checkin::whereDate('entry', '=', $now->format('Y-m-d'))
            ->where('userId', $userId)
            ->first();
        return $checkin;
    }

    public function updateStatus(Request $request)
    {
        $user = AuthController::getUser();
        if (!$user->can('update-checkins'))
            return response()->json([
                'success' => false,
            ], 400);
        $user = User::where('userName', $request->userName)->first();
        $checkin = $this->getStatus($user->userId);
        $now = Carbon::now()->setTimeZone('America/La_Paz');

        if ($checkin === null) {
            $checkin = new Checkin([
                'entry' => $now->format('Y-m-d h:i:s'),
                'status' => 'ENTRADA',
                'userId' => $user->userId
            ]);
            $message = "ENTRADA REGISTRADA CORRECTAMENTE";
        } else if ($checkin->status === "ENTRADA") {
            $checkin = Checkin::find($checkin->checkinId);
            $checkin->departure = $now->format('Y-m-d h:i:s');
            $checkin->status = "SALIDA";
            $message = "SALIDA REGISTRADA CORRECTAMENTE";
        } else {
            return response()->json(['success' => false, 'message' => 'EL USUARIO YA TIENE ENTRADA Y SALIDA REGISTRADAS'], 200);
        }
        $checkin->save();
        return response()->json(['success' => true, 'checkin' => $checkin, 'message' => $message], 200);
    }

    public function updateStatus2(Request $request)
    {
        $user = AuthController::getUser();
        if (!$user->can('update-checkins'))
            return response()->json([
                'success' => false,
            ], 400);
        $user = User::where('userName', $request->userName)->first();
        $checkin = $this->getStatus($user->userId);
        $now = Carbon::now()->setTimeZone('America/La_Paz');

        if ($checkin === null) {
            $checkin = new Checkin([
                'entry' => $now->format('Y-m-d h:i:s'),
                'status' => 'ENTRADA',
                'userId' => $user->userId
            ]);
            $message = "ENTRADA REGISTRADA CORRECTAMENTE";
            $checkin->save();
        } else {
            return response()->json(['success' => false, 'message' => 'EL USUARIO YA TIENE ENTRADA REGISTRADA'], 200);
        }

        return response()->json(['success' => true, 'checkin' => $checkin, 'message' => $message], 200);
    }

    public function read()
    {
        $user = AuthController::getUser();
        if (!$user->can('read-checkins'))
            return response()->json([
                'success' => false,
            ], 400);
        $now = Carbon::now()->setTimeZone('America/La_Paz');
        $checkins = Checkin::join('users', 'users.userId', 'checkins.userId')
            ->select('checkins.*', 'users.userName')
            ->whereDate('entry', '=', $now->format('Y-m-d'))->get();
        return response()->json(['success' => true, 'checkins' => $checkins], 200);
    }

    public function read2()
    {
        $user = AuthController::getUser();
        if (!$user->can('read-checkins'))
            return response()->json([
                'success' => false,
            ], 400);
        $now = Carbon::now()->setTimeZone('America/La_Paz');
        $users = User::join('profiles', 'profiles.userId', 'users.userId')
            ->select(DB::raw("CONCAT(profiles.name,' ',profiles.lastName) AS fullName"), 'users.userName', 'users.userId')
            ->orderBy('profiles.name')
            ->get();
        $checkins = Checkin::select('*')->whereDate('entry', '=', $now->format('Y-m-d'))->get();

        foreach ($checkins as $checkin) {
            for ($i = 0; $i < count($users); $i++) {
                if ($users[$i]->userId == $checkin->userId) {
                    $users[$i]->status = 'ENTRADA';
                    $users[$i]->entry = $checkin->entry;
                }
            }
        }

        /*$checkins = Checkin::rightJoin('users','users.userId','checkins.userId')
                        ->join('profiles','profiles.userId','users.userId')
                        ->select('checkins.*','users.userName',DB::raw("CONCAT(profiles.lastName,' ',profiles.name) AS fullName"))
                        //->whereDate('entry', '=', $now->format('Y-m-d'))
                        ->orderBy('profiles.lastName')
                        ->get();*/
        return response()->json(['success' => true, 'checkins' => $users], 200);
    }
}
