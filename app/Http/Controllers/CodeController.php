<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Code;
use App\Models\Save;
use App\Http\Controllers\CouponController;

class CodeController extends Controller
{
    private static function json(){//PRUEBAS
        $json = ([//aqui tenemos nuestras llaves
            'applicationCode' => '202110200400',
            'version' => 'v1',
            'secretKey' => 'andz1081',
            'reference' => 'sc-'
        ]);
        return $json;
    }


    private static function getJson(){
        $codesJson = Code::join('users','users.userId','=','codes.userId')
                    ->select('users.userName','codes.*')
                    ->where('codes.enabled','1')
                    ->orderBy('codes.codeId','desc')
                    ->get();
        return $codesJson;
    }

    public function create(Request $request){
        session_start();
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $code = new Code([
                'applicationCode' => self::json()['applicationCode'],
                'version' => self::json()['version'],
                'productCode' => $request->productCode,
                'quantity' => $request->quantity,
                'userId' => $user->userId
            ]);
            $code->save();
            $code = Code::all()->where('userId',$user->userId)->last();
            $code->referenceId = self::json()['reference'].$code->codeId;
            $signature = $code->applicationCode.$code->productCode.$code->quantity.$code->referenceId.$code->version.self::json()['secretKey'];
            $signature = md5($signature);
            $code->signature = $signature;
            $code->save();
            $code = new Code([
                'applicationCode' => $code->applicationCode,
                'version' => $code->version,
                'referenceId' => $code->referenceId,
                'productCode' => $code->productCode,
                'quantity' => $code->quantity,
                'signature' => $code->signature
            ]);
            $_SESSION['code'] = $code;
            return response()->json([
                'success' => true,
                'code' => $code
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function purchaseRequest(Request $request){
        session_start();
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $code = $_SESSION['code'];
            $code = Code::all()->where('referenceId',$code->referenceId)->first();
            $validatedToken = $request->validatedToken;
            $signature = $code->applicationCode.$code->referenceId.$code->version.$validatedToken.self::json()['secretKey'];
            $signature = md5($signature);
            $code->validatedToken = $validatedToken;
            $code->signature = $signature;
            $code->save();
            return response()->json([
                'success' => true,
                'code' => $code
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function reTryPurchaseRequest(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('create-codes'))
            return response()->json([
                'success' => false,
        ], 400);
        $code = Code::find($request->codeId);
        return response()->json([
            'success' => true,
            'code' => $code
        ], 200);
    }

    public function createNewCode(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $code = new Code([
                'applicationCode' => "-",
                'version' => "",
                'productCode' => "-",
                'quantity' => $request->quantity,
                'userId' => $user->userId,
                'productDescription' => $request->productDescription
            ]);
            $code->save();
            $codeId = $code->codeId;
            $code = Code::find($codeId);
            $quantity = CouponController::createPins($code->codeId,$request->pins);
            $code->status = '1';
            $code->deliveredQuantity = $quantity;
            $code->quantity = $quantity;
            $code->referenceId = self::json()['reference'].$codeId;
            $code->save();
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function updatePurchasedRequest(Request $request){
        session_start();
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $save = new Save(['referenceId' => $request->referenceId,'coupons' => $request->couponsString]);
            $save->save();
            $code = Code::all()->where('referenceId',$request->referenceId)->first();
            CouponController::createCoupons($code->codeId,$request->coupons);
            $userId = $code->userId;
            $code->fill($request->all());
            $code->status = "1";
            $code->deliveredQuantity = intval($request->deliveredQuantity);
            $code->recommendedRetailPrice = floatval($request->recommendedRetailPrice);
            $code->taxAmount = floatval($request->taxAmount);
            $code->userId = $userId;
            $code->save();////
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function read(){
        $user = AuthController::getUser();
        if(!$user->can('read-codes'))
            return response()->json([
                'success' => false,
        ], 400);
        $codesJson = self::getJson();
        return response()->json([
                'success' => true,
                'codesJson' => $codesJson
            ], 200);
    }

    public function findById(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $codeId = $request->codeId;
            $code = Code::all()->where('codeId',$codeId)->first();
            return response()->json([
                'success' => true,
                'code' => $code
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function deleteActualRequest(){
        session_start();
        try{
            $user = AuthController::getUser();
            if(!$user->can('delete-codes'))
                return response()->json([
                    'success' => false,
            ], 400);
            $code = $_SESSION['code'];
            $referenceId = $code->referenceId;
            $code = Code::all()->where('referenceId',$referenceId)->first();
            $code->enabled = '0';
            $code->save();
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public static function decreaseQuantity($codeId){
        try{
            $code = Code::all()->where('codeId',$codeId)->first();
            $quantity = intval($code->quantity);
            $quantity--;
            $code->quantity = $quantity;
            if($quantity == 0)
                $code->status = '2';
            $code->save();
            return $code;
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function getProducts(){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
            return response()->json([
                'success' => false,
        ], 400);
        $products = Code::select('productDescription')
                        ->groupBy('productDescription')
                        ->where('productDescription','<>','NULL')
                        ->where('version',"")
                        ->get();
        return response()->json([
                'success' => true,
                'products' => $products,
            ], 200);
    }
}
