<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdrawal;
use App\Models\Coupon;
use App\Models\WithdrawnCoupon;
use App\Http\Controllers\CodeController;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $withdrawal = new Withdrawal($request->all());
            $withdrawal->userId = $user->userId;
            $withdrawal->save();
            if(self::withdrawCoupons($request->productDescription,$withdrawal)){
                $couponsJSON = self::updateTXT($withdrawal->withdrawalId,$user->userId);
                return response()->json([
                    'success' => true,
                    'withdrawal' => $withdrawal
                ],
                200);
            }
            else{
                $withdrawal->delete();
                return response()->json(['success' => false,], 201);
            }
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    private static function withdrawCoupons($productDescription,$withdrawal){
        $coupons = Coupon::join('codes','codes.codeId','coupons.codeId')
                        ->select('coupons.*')
                        ->where('coupons.status','1')
                        ->where('codes.productDescription',$productDescription)
                        ->get();
        $quantity = $withdrawal->quantity;
        $withdrawalId = $withdrawal->withdrawalId;
        if($quantity <= count($coupons)){
            for($i=0;$i<$quantity;$i++){
                if($coupons[$i]->status == '1'){
                    $withdrawnCoupon = new WithdrawnCoupon([
                        'withdrawalId' => $withdrawalId,
                        'couponId' => $coupons[$i]->couponId
                    ]);
                    $coupons[$i]->status = '0';
                    $coupons[$i]->save();
                    CodeController::decreaseQuantity($coupons[$i]->codeId);
                    $withdrawnCoupon->save();
                }
            }
            return true;
        }
        return false;
    }

    public function read(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
        $couponsJSON = self::updateTXT($request->withdrawalId,$user->userId);
        return response()->json([
                    'success' => true,
                    'couponsJSON' => $couponsJSON
            ], 200);
    }

    public function readWithdrawals(){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
        $withdrawals = Withdrawal::join('withdrawn_coupons','withdrawn_coupons.withdrawalId','withdrawals.withdrawalId')
                                ->join('coupons','coupons.couponId','withdrawn_coupons.couponId')
                                ->join('codes','codes.codeId','coupons.codeId')
                                ->select('withdrawals.withdrawalId','withdrawals.quantity','withdrawals.name','withdrawals.created_at','codes.productDescription')
                                ->where('withdrawals.userId',$user->userId)
                                ->groupBy('withdrawals.withdrawalId','withdrawals.quantity','withdrawals.name','withdrawals.created_at','codes.productDescription')
                                ->orderByDesc('withdrawals.withdrawalId')
                                ->get();
        return response()->json([
                    'success' => true,
                    'withdrawals' => $withdrawals
            ], 200);
    }

    private static function getJSON($withdrawalId,$userId){
        $couponsJSON = Withdrawal::join('withdrawn_coupons','withdrawn_coupons.withdrawalId','withdrawals.withdrawalId')
                                    ->join('coupons','coupons.couponId','withdrawn_coupons.couponId')
                                    ->join('codes','codes.codeId','coupons.codeId')
                                    ->select('codes.productDescription',DB::raw("CONCAT(coupons.pin,'-') as pin"))
                                    ->where('withdrawn_coupons.withdrawalId',$withdrawalId)
                                    ->where('withdrawals.userId',$userId)
                                    ->get();
        return $couponsJSON;
    }

    protected static function updateTXT($withdrawalId,$userId){
        $coupons = Withdrawal::join('withdrawn_coupons','withdrawn_coupons.withdrawalId','withdrawals.withdrawalId')
                                    ->join('coupons','coupons.couponId','withdrawn_coupons.couponId')
                                    ->join('codes','codes.codeId','coupons.codeId')
                                    ->select('codes.productDescription','coupons.pin')
                                    ->where('withdrawn_coupons.withdrawalId',$withdrawalId)
                                    ->where('withdrawals.userId',$userId)
                                    ->get();
        try{
            $src = "\\secret-carpet789\\coupons.txt";
            $pathtoFile = storage_path().$src;
            $file = fopen($pathtoFile, "w+b");
            fwrite($file, $coupons[0]->productDescription."\n");
            for($i=0;$i<count($coupons);$i++){
                $n = $i+1;
                $row = "$n. ".$coupons[$i]->pin."\n";
                fwrite($file, $row);
            }
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    /*protected static function updateTXT($withdrawalId,$userId){//ventas vip
        $coupons = Withdrawal::join('withdrawn_coupons','withdrawn_coupons.withdrawalId','withdrawals.withdrawalId')
                                    ->join('coupons','coupons.couponId','withdrawn_coupons.couponId')
                                    ->join('codes','codes.codeId','coupons.codeId')
                                    ->select('codes.productDescription','coupons.pin')
                                    ->where('withdrawn_coupons.withdrawalId',$withdrawalId)
                                    ->where('withdrawals.userId',$userId)
                                    ->get();
        try{
            $src = "\\secret-carpet789\\coupons.txt";
            $pathtoFile = storage_path().$src;
            $file = fopen($pathtoFile, "w+b");
            $pinTitle = $coupons[0]->productDescription;
            fwrite($file,"🇧🇴 𝐕𝐄𝐍𝐓𝐀𝐒 𝐎𝐍𝐋𝐈𝐍𝐄 𝐕𝐈𝐏 𝐁𝐎𝐋𝐈𝐕𝐈𝐀 🇧🇴\n");
            fwrite($file,"💲𝐆𝐀𝐌𝐄𝐋𝐎𝐀𝐃 𝐁𝐎𝐋𝐈𝐕𝐈𝐀💲\n");
            fwrite($file, "*$pinTitle*\n");
            for($i=0;$i<count($coupons);$i++){
                $n = $i+1;
                $initials = "";
                if(explode("Razer Gold Direct Top-Up PIN (",$pinTitle)[0] == ""){
                    $initials = " *".explode(")",explode("(",$pinTitle)[1])[0]."*";
                    if($initials == " *CO*")
                        $initials = " *COP*";
                }
                $row = "$n;$initials ➤ *".$coupons[$i]->pin."*\n";
                fwrite($file, $row);
            }
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }*/

    protected static function clearCache(){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
        $src = "\\secret-carpet789";
        $pathtoFile = storage_path().$src;
        $files = glob($pathtoFile.'\\*'); //obtenemos todos los nombres de los ficheros
        foreach($files as $file){
            if(is_file($file))
                unlink($file); //elimino el fichero
        }
    }

    protected static function findById(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
        $withdrawnCoupons = Withdrawal::join('withdrawn_coupons','withdrawn_coupons.withdrawalId','withdrawals.withdrawalId')
                                ->join('coupons','coupons.couponId','withdrawn_coupons.couponId')
                                ->join('codes','codes.codeId','coupons.codeId')
                                ->select('withdrawals.withdrawalId','withdrawals.quantity','withdrawals.name','withdrawals.created_at','codes.productDescription','coupons.pin')
                                ->where('withdrawals.userId',$user->userId)
                                ->where('withdrawals.withdrawalId',$request->withdrawalId)
                                ->orderByDesc('withdrawals.withdrawalId')
                                ->get();
        return response()->json([
                    'success' => true,
                    'withdrawnCoupons' => $withdrawnCoupons
            ], 200);
    }

    protected static function updateDownload(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
        self::updateTXT($request->withdrawalId,$user->userId);
        return response()->json([
                    'success' => true
            ], 200);
    }
}

