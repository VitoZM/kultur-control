<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Code;
use App\Models\Coupon;
use App\Models\Withdrawal;
use App\Http\Controllers\CodeController;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{

    public static function uploadtxt(){
        $user = AuthController::getUser();
        if(!$user->can('update-coupons'))
            return response()->json(['success' => false,], 400);
        $arr_file_types = ['text/plain'];
        //return $_FILES['file']['type'];
        if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
            echo "false";
            return;
        }
            
        if (!file_exists('uploads')) {
            mkdir('uploads', 0777);
        }
            
        $filename = time().'_'.$_FILES['file']['name'];
            
        move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/'.$filename);
            
        echo 'uploads/'.$filename;
        die;
    }

    public static function createMassiveCoupons(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('update-coupons'))
            return response()->json(['success' => false], 400);
        
        $lastCode = Code::all()->last();
        $codeId = $lastCode->codeId + 1;
        $productDescription = $request->productDescription;
        $code = new Code([
            'codeId' => $codeId,
            'referenceId' => "gxy-".$codeId,
            'paymentId' => '-',
            'productCode' => '-',
            'quantity' => 1,
            'deliveredQuantity' => 1,
            'currency' => 'COP',
            'unitPrice' => 1,
            'totalPrice' => 1,
            'purchaseStatusCode' => "00",
            'productDescription' => $productDescription,
            'totalPayablePrice' => 1,
            'taxAmount' => 0,
            'isReversible' => true,
            'version' => 'v1',
            'signature' => '-',
            'validatedToken' => '-',
            'applicationCode' => "-",
            'status' => '1',
            'userId' => $user->userId
        ]);
        $count = 0;
        $code->save();
        $lastCode = Code::all()->last();
        $lines = $request->lines;
        foreach($lines as $line){
            $count++;
            //$linea = explode(" ",$linea)[3];//por si tiene espacios los pines
            $coupon = new Coupon([
                'expiryDate' => '2030-12-31',
                'serial' => '-',
                'pin' => $line,
                'status' => '1',
                'codeId' => $lastCode->codeId
            ]);
            $coupon->save();
        }
        $lastCode->quantity = $count;
        $lastCode->deliveredQuantity = $count;
        $lastCode->save();
        return response()->json(['success' => true], 200);
    }

    private static function getJson(){
        $couponsJson = Coupon::join('codes','codes.codeId','=','coupons.codeId')
                    ->select('referenceId','productCode','productDescription','coupons.*')
                    ->where('codes.enabled','1')
                    ->orderBy('codes.codeId','desc')
                    ->get();
        return $couponsJson;
    }

    public function findById(Request $request){
        try{
            $coupon = Coupon::join('codes','codes.codeId','=','coupons.codeId')
                    ->select('referenceId','productCode','recommendedRetailPrice','productDescription','currency','coupons.*')
                    ->where('coupons.couponId',$request->couponId)
                    ->first();
            return response()->json([
                'success' => true,
                'coupon' => $coupon,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public static function createCoupons($codeId,$coupons){
        try{
            foreach($coupons as $coupon){
                $expiryDate = $coupon['expiryDate'];
                $codeId = $codeId;
                $serials = $coupon['serials'];
                $pins = $coupon['pins'];
                $serial = "";
                $pin = "";
                for($i=0;$i<count($serials);$i++){
                    if($i>0)
                        $serial.="¡-!";
                    $serial.=$serials[$i];
                }
                for($i=0;$i<count($pins);$i++){
                    if($i>0)
                        $pin.="¡-!";
                    $pin.=$pins[$i];
                }
                $actualCoupon = new Coupon([
                    'expiryDate' => $coupon['expiryDate'],
                    'serial' => $serial,
                    'pin' => $pin,
                    'codeId' => $codeId
                ]);
                $actualCoupon->save();
            }
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public static function createPins($codeId,$pins){
        try{
            $quantity = 0;
            for($i=0;$i<count($pins);$i++){
                $pin = $pins[$i];
                if($pin == "")
                    continue;
                $quantity++;
                $expiryDate = "2030/01/01 00:00:00";
                $serial = "-";
                $codeId = $codeId;
                $actualCoupon = new Coupon([
                    'expiryDate' => $expiryDate,
                    'serial' => $serial,
                    'pin' => $pin,
                    'codeId' => $codeId
                ]);
                $actualCoupon->save();
            }
            return $quantity;
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-coupons'))
                return response()->json([
                    'success' => false,
            ], 400);
            $coupon = Coupon::all()->where('couponId',$request->couponId)->first();
            $coupon->status = '0';
            $coupon->save();
            CodeController::decreaseQuantity($coupon->codeId);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }

    public function multipleUpdate(Request $request){
        try{
            $couponArray = $request->couponArray;
            $file = fopen("files/coupons.txt", "w+b");

            foreach($couponArray as $couponId){
                $coupon = Coupon::all()->where('couponId',$couponId)->first();
                $coupon->status = '0';
                $coupon->save();
                $code = CodeController::decreaseQuantity($coupon->codeId);
                fwrite($file, $coupon->pin." | ".$code->productDescription."\n");
            }
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
            return response()->json(['success' => true], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        finally{
            fclose($file);
        }
    }

    public function read(){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
            return response()->json([
                'success' => false,
        ], 400);
        $couponsJson = self::getJson();
        return response()->json([
                'success' => true,
                'couponsJson' => $couponsJson,
            ], 200);
    }

    public function getAvailableProducts(){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
            return response()->json([
                'success' => false,
        ], 400);
        $products = Coupon::join('codes','codes.codeId','coupons.codeId')
                        ->select('codes.productDescription',DB::raw('COUNT(coupons.codeId) as quantity'))
                        ->where('coupons.status','1')
                        ->groupBy('codes.productDescription')
                        ->get();
        return response()->json([
                'success' => true,
                'products' => $products,
            ], 200);
    }

    public function getQuantity(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-coupons'))
            return response()->json([
                'success' => false,
        ], 400);
        $product = Coupon::join('codes','codes.codeId','coupons.codeId')
                        ->select('codes.productDescription',DB::raw('COUNT(coupons.codeId) as quantity'))
                        ->where('coupons.status','1')
                        ->where('codes.productDescription',$request->productDescription)
                        ->groupBy('codes.productDescription')
                        ->first();
        return response()->json([
                'success' => true,
                'product' => $product,
            ], 200);
    }

    public function clean(){
        $fileNames = [
            'app-assets/data/bitacora-list.json',
            'app-assets/data/codes-list.json',
            'app-assets/data/coupons-list.json',
            'app-assets/data/users-list.json',
            'app-assets/data/withdrawn-coupons.json',
            'files/coupons.txt'
        ];
        foreach($fileNames as $fileName)
            self::rewrite($fileName);
    }

    private static function rewrite($fileName){
        try{
            $file = fopen($fileName, "w+b");
            fwrite($file,"test.json");
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        finally{
            fclose($file);
        }
    }

    protected function download($fileName,$token){
        if(!auth()->user()){
            abort(404,'Page not found');
            abort(403);
        }
        if($fileName == "" || $fileName == null)
            $fileName = "Pines";
        $src = storage_path()."\\secret-carpet789\\coupons.txt";
        $pathtoFile = storage_path()."\\secret-carpet789\\$fileName.txt";
        rename($src,$pathtoFile);
        return response()->download($pathtoFile);
    }

    protected function firstDownload($fileName,$token){
        if(!auth()->user()){
            abort(404,'Page not found');
            abort(403);
        }
        if($fileName == "" || $fileName == null)
            $fileName = "Pines";
        $withdrawal = Withdrawal::all()->where('name','Pines')->last();
        $withdrawal->name = $fileName;
        $withdrawal->save();
        $src = storage_path()."\\secret-carpet789\\coupons.txt";
        $pathtoFile = storage_path()."\\secret-carpet789\\$fileName.txt";
        rename($src,$pathtoFile);
        return response()->download($pathtoFile);
    }

}
