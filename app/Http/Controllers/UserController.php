<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\CheckinController;

class UserController extends Controller
{
    public static function getJson(){
        $usersJson = User::join('profiles','users.userId','=','profiles.userId')
                        ->select('profiles.*','users.userName')
                        ->where('users.enabled','1')
                        ->get();
        return $usersJson;
    }

    public static function findByUserName(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-users'))
            return response()->json([
                'success' => false,
        ], 400);
        $userName = strtolower($request->userName);
        $user = User::where('userName',$userName)->first();
        if($user != null)
            return response()->json(['user' => $user], 200);
        else
            return response()->json('Not Found',500);
    }

    public static function getProfileAndStatus(Request $request){
        $user = AuthController::getUser();
        if(!$user->can('read-users'))
            return response()->json([
                'success' => false,
        ], 400);
        $user = User::join('profiles','profiles.userId','users.userId')
                    ->select('profiles.*','users.userName')
                    ->where('users.userName',$request->userName)
                    ->first();
        $checkin = CheckinController::getStatus($user->userId);
        if($checkin != null)
            $status = $checkin->status;
        else
            $status = null;
        return response()->json(['success' => true, 'user' => $user, 'status' => $status], 200);
    }

    public function createUserProfile($request){
        $profile = new Profile($request->all());
        $insertedUser = User::all()->last();
        $profile->userId = $insertedUser->userId;
        $profile->save();
    }

    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $user = new User($request->all());
            $user->userName = strtolower($user->userName);
            $user->password = Hash::make($request->password);
            $user->save();
            $role = Role::find(2);
            $user->assignRole($role->name);
            self::createUserProfile($request);
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        $user = AuthController::getUser();
        if(!$user->can('read-users'))
            return response()->json([
                'success' => false,
        ], 400);
        $usersJson = self::getJson();
        return response()->json([
            'success' => true,
            'usersJson' => $usersJson
        ], 200);
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            if($user->userId != $userId)
                return response()->json(['success' => false], 501);
            $newPassword = $request->newPassword;
            $confirmPassword = $request->confirmPassword;
            if($newPassword != $confirmPassword)
                return response()->json(['success' => false], 502);
            $actualPassword = $request->actualPassword;
            $rowUser = User::all()->where('userId',$user->userId)->first();
            if(!Hash::check($actualPassword, $rowUser->password))
                return response()->json(['success' => false,'acPas' => $actualPassword,'usPas' => $rowUser->password], 502);

            $rowUser->password = Hash::make($newPassword);
            $rowUser->save();
            return response()->json(['success' => true], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('delete-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $user = User::all()->where("userId",$userId)->first();
            $user->enabled = "0";
            $user->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('userName', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('user'));
    }

    public function quantity(){
        $count = User::select('*')
                        ->where('enabled','1')
                        ->get()
                        ->count();

        return $count;
    }
}
