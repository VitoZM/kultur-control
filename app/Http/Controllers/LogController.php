<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    private static function getJson(){
        $logsJson = Log::join('users','users.userId','=','logs.userId')
                    ->select('users.userName','logs.*')
                    ->orderBy('logs.logId','desc')
                    ->get();
        return $logsJson;
    }

    public function create(){
        try{
            $now = new \DateTime();
            $user = AuthController::getUser();
            $lastLog = Log::all()->where("userId",$user->userId)->last();
            if($lastLog != null){
                if($lastLog->status == "1")
                    self::update();
            }
            $now = $now->format('Y-m-d H:i:s');
            $log = new Log([
                'entry' => $now,
                'departure' => $now,
                'userId' => $user->userId
            ]);
            $log->save();
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        return $user;
    }

    public function update(){
        try{
            $now = new \DateTime();
            $departure = $now->format('Y-m-d H:i:s');
            $user = AuthController::getUser();
            $log = Log::all()->where("userId",$user->userId)->last();
            $log->departure = $departure;
            $log->status = "0";
            $entry = new \DateTime($log->entry);
            $duration = $entry->diff($now);
            $log->duration = $duration->format('%m meses %d days %h horas %i minutos %s segundos');
            $log->save();
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        return $user;
    }

    public function read(){
        $user = AuthController::getUser();
        if(!$user->can('read-logs'))
            return response()->json([
                'success' => false,
        ], 400);
        $logsJson = self::getJson();
        return response()->json([
                'success' => true,
                'logsJson' => $logsJson
            ], 200);
    }
}
