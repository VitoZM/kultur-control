<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'couponId',
        'expiryDate',
        'serial',
        'pin',
        'status',
        'codeId'
    ];

    protected $primaryKey = 'couponId';
}
