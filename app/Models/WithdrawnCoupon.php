<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawnCoupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'withdrawnCouponId',
        'withdrawalId',
        'couponId'
    ];

    protected $primaryKey = 'withdrawnCouponId';
}
