<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'userId' => 1,
            'name' => 'Alvaro',
            'lastName' => 'Zapata',
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);

        /*DB::table('profiles')->insert([
            'userId' => 2,
            'name' => 'Jackson',
            'lastName' => 'Villca',
            'created_at' => '2021/01/14 08:00:00',
            'updated_at' => '2021/01/14 08:00:00'
        ]);*/

        DB::table('profiles')->insert([
            'userId' => 2,
            'name' => 'Verónica',
            'lastName' => ' ',
            'created_at' => '2021/04/09 08:00:00',
            'updated_at' => '2021/04/09 08:00:00'
        ]);
    }
}
