<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'userId' => 1,
            'userName' => '10331470',
            'password' => Hash::make('AdminSuperG#5.'),//$2y$10$xKI8ExVdpu8yPgsgmlYtBefWigJbSyh6./coS5zshDl/Yzx22OhZS
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('users')->insert([
            'userId' => 2,
            'userName' => 'verito',
            'password' => Hash::make('verito.Pass7'),
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
