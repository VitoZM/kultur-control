<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\CheckinController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// @TODO: Auth
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});

Route::post('/log/create', [LogController::class, 'create']);
Route::post('/log/read', [LogController::class, 'read']);
Route::post('/log/update', [LogController::class, 'update']);

Route::post('/user/create', [UserController::class, 'create']);
Route::post('/user/read', [UserController::class, 'read']);
Route::post('/user/update', [UserController::class, 'update']);
Route::post('/user/delete', [UserController::class, 'delete']);
Route::post('/user/findByUserName', [UserController::class, 'findByUserName']);
Route::post('/user/getProfileAndStatus', [UserController::class, 'getProfileAndStatus']);

Route::post('/profile/update', [ProfileController::class, 'update']);
Route::post('/profile/getProfile', [ProfileController::class, 'getProfile']);

Route::post('/checkin/updateStatus', [CheckinController::class, 'updateStatus']);
Route::post('/checkin/read', [CheckinController::class, 'read']);
Route::post('/checkin/read2', [CheckinController::class, 'read2']);
Route::post('/checkin/updateStatus2', [CheckinController::class, 'updateStatus2']);

