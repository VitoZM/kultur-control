function fillWithdrawals(gridData) {
    var isRtl;
    if ($("html").attr("data-textdirection") == "rtl") {
        isRtl = true;
    } else {
        isRtl = false;
    }

    var customIconsHTML = function (params) {
        let withdrawalId = params.data.withdrawalId;
        var withdrawalsIcons = document.createElement("span");
        var viewIconHTML = `<a href='#' onclick="fillViewModal(${withdrawalId});"><i class= 'withdrawals-view-icon text-danger feather icon-eye mr-50'></i></a>`;
        var downloadIconHTML = `<a href='#' onclick="downloadCoupons(${withdrawalId});"><i class= 'withdrawals-download-icon  feather icon-download'></i></a>`;

        withdrawalsIcons.appendChild($.parseHTML(viewIconHTML)[0]);
        withdrawalsIcons.appendChild($.parseHTML(downloadIconHTML)[0]);
        return withdrawalsIcons;
    };

    function getDay(date) {
        let day = date.getDate();
        return parseInt(day) < 10 ? "0" + day : day;
    }

    function getMonth(date) {
        let month = parseInt(date.getMonth()) + 1;
        return month < 10 ? "0" + month : month;
    }

    function getHours(date) {
        let hours = date.getHours();
        return parseInt(hours) < 10 ? "0" + hours : hours;
    }

    function getMinutes(date) {
        let minutes = date.getMinutes();
        return parseInt(minutes) < 10 ? "0" + minutes : minutes;
    }

    var idFormat = function (params) {
        return "RE-" + params.value;
    };

    var dateFormat = function (params) {
        let date = new Date(params.value);
        let day = getDay(date);
        let month = getMonth(date);
        let year = date.getFullYear();
        let hours = getHours(date);
        let minutes = getMinutes(date);

        return `${day}/${month}/${year} ${hours}:${minutes}`;
    };

    // ag-grid
    /*** COLUMN DEFINE ***/

    var columnDefs = [
        {
            headerName: "N° De Retiro",
            field: "withdrawalId",
            filter: true,
            width: 100,
            cellRenderer: idFormat,
            cellStyle: {
                "text-align": "center",
            },
        },
        {
            headerName: "Producto",
            field: "productDescription",
            filter: true,
            width: 400,
        },
        {
            headerName: "Cantidad",
            field: "quantity",
            filter: true,
            width: 150,
            cellStyle: {
                "text-align": "center",
            },
        },
        {
            headerName: "Nombre",
            field: "name",
            filter: true,
            width: 150,
            cellStyle: {
                "text-align": "center",
            },
        },
        {
            headerName: "Fecha",
            field: "created_at",
            filter: true,
            width: 200,
            cellRenderer: dateFormat,
        },
        {
            headerName: "Acciones",
            field: "acciones",
            width: 125,
            cellRenderer: customIconsHTML,
            cellStyle: {
                "text-align": "center",
            },
        },
    ];

    /*** GRID OPTIONS ***/
    gridOptions = {
        defaultColDef: {
            sortable: true,
        },
        enableRtl: isRtl,
        columnDefs: columnDefs,
        rowSelection: "single",
        // floatingFilter: true,
        filter: true,
        pagination: true,
        paginationPageSize: 20,
        pivotPanelShow: "always",
        colResizeDefault: "shift",
        animateRows: true,
        resizable: true,
    };
    if (document.getElementById("myGrid")) {
        /*** DEFINED TABLE VARIABLE ***/
        var gridTable = document.getElementById("myGrid");

        /*** ponemos los datos del JSON en la tabla***/
        agGrid
            .simpleHttpRequest({
                url: "app-assets/data/json.json",
            })
            .then(function (data) {
                gridOptions.api.setRowData(gridData);
            });

        /*** FILTER TABLE ***/
        function updateSearchQuery(val) {
            gridOptions.api.setQuickFilter(val);
        }

        $(".ag-grid-filter").on("keyup", function () {
            updateSearchQuery($(this).val());
        });

        /*** CHANGE DATA PER PAGE ***/
        function changePageSize(value) {
            gridOptions.api.paginationSetPageSize(Number(value));
        }

        $(".sort-dropdown .dropdown-item").on("click", function () {
            var $this = $(this);
            changePageSize($this.text());
            $(".filter-btn").text("1 - " + $this.text() + " de 50");
        });

        /*** EXPORT AS CSV BTN ***/
        $(".ag-grid-export-btn").on("click", function (params) {
            gridOptions.api.exportDataAsCsv();
        });

        //  filter data function
        var filterData = function agSetColumnFilter(column, val) {
            var filter = gridOptions.api.getFilterInstance(column);
            var modelObj = null;
            if (val !== "all") {
                modelObj = {
                    type: "equals",
                    filter: val,
                };
            }
            filter.setModel(modelObj);
            gridOptions.api.onFilterChanged();
        };
        //  filter inside role
        $("#users-list-role").on("change", function () {
            var usersListRole = $("#users-list-role").val();
            filterData("role", usersListRole);
        });
        //  filter inside verified
        $("#users-list-verified").on("change", function () {
            var usersListVerified = $("#users-list-verified").val();
            filterData("is_verified", usersListVerified);
        });
        //  filter inside status
        $("#users-list-status").on("change", function () {
            var usersListStatus = $("#users-list-status").val();
            filterData("status", usersListStatus);
        });
        //  filter inside department
        $("#users-list-department").on("change", function () {
            var usersListDepartment = $("#users-list-department").val();
            filterData("department", usersListDepartment);
        });
        // filter reset
        $(".users-data-filter").click(function () {
            $("#users-list-role").prop("selectedIndex", 0);
            $("#users-list-role").change();
            $("#users-list-status").prop("selectedIndex", 0);
            $("#users-list-status").change();
            $("#users-list-verified").prop("selectedIndex", 0);
            $("#users-list-verified").change();
            $("#users-list-department").prop("selectedIndex", 0);
            $("#users-list-department").change();
        });

        /*** INIT TABLE ***/
        new agGrid.Grid(gridTable, gridOptions);
    }
    // users language select
    if ($("#users-language-select2").length > 0) {
        $("#users-language-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users music select
    if ($("#users-music-select2").length > 0) {
        $("#users-music-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users movies select
    if ($("#users-movies-select2").length > 0) {
        $("#users-movies-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users birthdate date
    if ($(".birthdate-picker").length > 0) {
        $(".birthdate-picker").pickadate({
            format: "d,mmmm, yyyy",
        });
    }
    // Input, Select, Textarea validations except submit button validation initialization
    if ($(".users-edit").length > 0) {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }
}
