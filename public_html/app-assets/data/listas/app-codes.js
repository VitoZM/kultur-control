function fillCodes(gridData) {
    var isRtl;
    if ($("html").attr("data-textdirection") == "rtl") {
        isRtl = true;
    } else {
        isRtl = false;
    }

    //  Rendering badge in status column
    var customBadgeHTML = function (params) {
        var color = "";
        if (params.value == "1") {
            color = "success";
            return (
                "<div class='badge badge-pill badge-light-" +
                color +
                "' >Disponible</div>"
            );
        } else if (params.value == "0") {
            color = "warning";
            return (
                "<div class='badge badge-pill badge-light-" +
                color +
                "' >En Proceso</div>"
            );
        } else if (params.value == "2") {
            color = "danger";
            return (
                "<div class='badge badge-pill badge-light-" +
                color +
                "' >Agotado</div>"
            );
        }
    };

    //  Rendering bullet in verified column
    var customBulletHTML = function (params) {
        var color = "";
        if (params.value == true) {
            color = "success";
            return (
                "<div class='bullet bullet-sm bullet-" +
                color +
                "' >" +
                "</div>"
            );
        } else if (params.value == false) {
            color = "secondary";
            return (
                "<div class='bullet bullet-sm bullet-" +
                color +
                "' >" +
                "</div>"
            );
        }
    };

    // Renering Icons in Actions column
    var customIconsHTML = function (params) {
        var usersIcons = document.createElement("span");
        usersIcons.id = "eliminarRegistro";
        var editIconHTML = `<a href='#' data-toggle='modal' data-target='#editarPrecio' onclick="fillEditModal(${params.data.pricePerMeterId})">
                                <i class= 'users-edit-icon text-primary feather icon-edit-1 mr-50'></i>
                            </a>`;
        var eyeIconHTML = `<a href='#' data-toggle='modal' data-target='#eliminarMedidor' onclick='showModal(event,${params.data.codeId})'><i class= 'users-delete-icon text-danger feather icon-eye'></i></a>`;

        //usersIcons.appendChild($.parseHTML(editIconHTML)[0]);
        usersIcons.appendChild($.parseHTML(eyeIconHTML)[0]);
        return usersIcons;
    };

    var bolivianDate = function (params) {
        let date = new Date(params.value);
        let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        let month =
            date.getMonth() + 1 < 10
                ? "0" + (parseInt(date.getMonth()) + 1)
                : date.getMonth();
        let year = date.getFullYear();
        let hours =
            date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        let minutes =
            date.getMinutes() < 10
                ? "0" + date.getMinutes()
                : date.getMinutes();
        let seconds =
            date.getSeconds() < 10
                ? "0" + date.getSeconds()
                : date.getSeconds();
        return `${day}/${month}/${year} ${hours}:${minutes}:${seconds}`;
    };

    // ag-grid
    /*** COLUMN DEFINE ***/

    var columnDefs = [
        {
            headerName: "Referencia",
            field: "referenceId",
            width: 150,
            filter: true,
        },
        {
            headerName: "Código Producto",
            field: "productCode",
            filter: true,
            width: 150,
        },
        {
            headerName: "Descripción",
            field: "productDescription",
            filter: true,
            width: 350,
        },
        {
            headerName: "Fecha de Creación",
            field: "created_at",
            cellRenderer: bolivianDate,
            filter: true,
            width: 220,
        },
        {
            headerName: "Disponible",
            field: "quantity",
            filter: true,
            width: 100,
        },
        {
            headerName: "Estado",
            field: "status",
            filter: true,
            cellRenderer: customBadgeHTML,
            width: 120,
        },
        {
            headerName: "Acciones",
            field: "acciones",
            width: 120,
            cellRenderer: customIconsHTML,
            cellStyle: {
                "text-align": "center",
            },
        },
    ];

    /*** GRID OPTIONS ***/
    var gridOptions = {
        defaultColDef: {
            sortable: true,
        },
        enableRtl: isRtl,
        columnDefs: columnDefs,
        rowSelection: "single",
        // floatingFilter: true,
        filter: true,
        pagination: true,
        paginationPageSize: 20,
        pivotPanelShow: "always",
        colResizeDefault: "shift",
        animateRows: true,
        resizable: true,
    };
    if (document.getElementById("myGrid")) {
        /*** DEFINED TABLE VARIABLE ***/
        var gridTable = document.getElementById("myGrid");
        document.getElementById("myGrid").innerHTML = "";
        /*** ponemos los datos del JSON en la tabla***/
        agGrid
            .simpleHttpRequest({
                url: "app-assets/data/json.json",
            })
            .then(function (data) {
                gridOptions.api.setRowData(gridData);
            });

        /*** FILTER TABLE ***/
        function updateSearchQuery(val) {
            gridOptions.api.setQuickFilter(val);
        }

        $(".ag-grid-filter").on("keyup", function () {
            updateSearchQuery($(this).val());
        });

        /*** CHANGE DATA PER PAGE ***/
        function changePageSize(value) {
            gridOptions.api.paginationSetPageSize(Number(value));
        }

        $(".sort-dropdown .dropdown-item").on("click", function () {
            var $this = $(this);
            changePageSize($this.text());
            $(".filter-btn").text("1 - " + $this.text() + " de 50");
        });

        /*** EXPORT AS CSV BTN ***/
        $(".ag-grid-export-btn").on("click", function (params) {
            gridOptions.api.exportDataAsCsv();
        });

        //  filter data function
        var filterData = function agSetColumnFilter(column, val) {
            var filter = gridOptions.api.getFilterInstance(column);
            var modelObj = null;
            if (val !== "all") {
                modelObj = {
                    type: "equals",
                    filter: val,
                };
            }
            filter.setModel(modelObj);
            gridOptions.api.onFilterChanged();
        };
        //  filter inside role
        $("#users-list-role").on("change", function () {
            var usersListRole = $("#users-list-role").val();
            filterData("role", usersListRole);
        });
        //  filter inside verified
        $("#users-list-verified").on("change", function () {
            var usersListVerified = $("#users-list-verified").val();
            filterData("is_verified", usersListVerified);
        });
        //  filter inside status
        $("#users-list-status").on("change", function () {
            var usersListStatus = $("#users-list-status").val();
            filterData("status", usersListStatus);
        });
        //  filter inside department
        $("#users-list-department").on("change", function () {
            var usersListDepartment = $("#users-list-department").val();
            filterData("department", usersListDepartment);
        });
        // filter reset
        $(".users-data-filter").click(function () {
            $("#users-list-role").prop("selectedIndex", 0);
            $("#users-list-role").change();
            $("#users-list-status").prop("selectedIndex", 0);
            $("#users-list-status").change();
            $("#users-list-verified").prop("selectedIndex", 0);
            $("#users-list-verified").change();
            $("#users-list-department").prop("selectedIndex", 0);
            $("#users-list-department").change();
        });

        /*** INIT TABLE ***/
        new agGrid.Grid(gridTable, gridOptions);
    }
    // users language select
    if ($("#users-language-select2").length > 0) {
        $("#users-language-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users music select
    if ($("#users-music-select2").length > 0) {
        $("#users-music-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users movies select
    if ($("#users-movies-select2").length > 0) {
        $("#users-movies-select2").select2({
            dropdownAutoWidth: true,
            width: "100%",
        });
    }
    // users birthdate date
    if ($(".birthdate-picker").length > 0) {
        $(".birthdate-picker").pickadate({
            format: "mmmm, d, yyyy",
        });
    }
    // Input, Select, Textarea validations except submit button validation initialization
    if ($(".users-edit").length > 0) {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }
}
