var gridOptions;
isLogged("read-coupons");
readAvailableProducts();
clearCache();
function clearCache() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/clearCache";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {},
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function readAvailableProducts() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/coupon/getAvailableProducts";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#productDescriptions").empty();
                $("#quantity").attr("readonly", "true");
                $("#quantity").val("");
                $("#availableQuantity").val("");
                $("#withdrawBtn").attr("disabled", "true");
                $option = $("<option />", {
                    text: "",
                    value: "",
                    selected: "true",
                });
                $("#productDescriptions").prepend($option);
                let products = response.products;
                products.forEach((product) => {
                    $option = $("<option />", {
                        text: product.productDescription,
                        value: product.productDescription,
                    });
                    $("#productDescriptions").prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#productDescriptions").on("change", function () {
    let productDescription = $("#productDescriptions").val();
    getQuantity(productDescription);
});

function getQuantity(productDescription) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = { productDescription: productDescription };
    let url = "api/coupon/getQuantity";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                let product = response.product;
                $("#availableQuantity").val(product.quantity);
                $("#quantity").removeAttr("readonly");
                $("#withdrawBtn").removeAttr("disabled");
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#withdrawBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let quantityError = validateQuantity();
    let quantityError2 = validateQuantity2();
    let productCodeError = validateProductDescriptions();

    if (productCodeError || quantityError || quantityError2) return true;
    return false;
}

function validateQuantity() {
    let quantity = $("#quantity").val().trim();
    if (quantity == "") {
        $("#quantityErrorMessage").removeClass("d-none");
        $("#quantityErrorMessage").addClass("d-block");
        $("#quantity").focus();
        return true;
    } else {
        $("#quantityErrorMessage").removeClass("d-block");
        $("#quantityErrorMessage").addClass("d-none");
        return false;
    }
}

function validateQuantity2() {
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    let availableQuantity = $("#availableQuantity").val().trim();
    availableQuantity = parseInt(availableQuantity);
    if (quantity < 1 || quantity > availableQuantity) {
        $("#quantityErrorMessage2").removeClass("d-none");
        $("#quantityErrorMessage2").addClass("d-block");
        $("#quantity").focus();
        return true;
    } else {
        $("#quantityErrorMessage2").removeClass("d-block");
        $("#quantityErrorMessage2").addClass("d-none");
        return false;
    }
}

function validateProductDescriptions() {
    let productDescription = $("#productDescriptions").val();
    if (productDescription == null) {
        $("#productDescriptionsErrorMessage").removeClass("d-none");
        $("#productDescriptionsErrorMessage").addClass("d-block");
        return true;
    } else {
        $("#productDescriptionsErrorMessage").removeClass("d-block");
        $("#productDescriptionsErrorMessage").addClass("d-none");
        return false;
    }
}

function create() {
    $("#loading").removeClass("d-none");
    $("#loading").fadeIn(500);
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    let productDescription = $("#productDescriptions").val();
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let data = {
        productDescription: productDescription,
        quantity: quantity,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/create";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                /*let json = response.couponsJSON;
                let productDescription = json[0].productDescription;
                fillCoupons(json);*/
                Swal.fire({
                    type: "success",
                    title: "INGRESE NOMBRE DEL ARCHIVO",
                    input: "text",
                    confirmButtonText: `Descargar`,
                }).then((result) => {
                    let fileName = result.value;
                    if (
                        fileName == "" ||
                        fileName == undefined ||
                        fileName == null
                    )
                        fileName = "Pines";
                    window.location = `coupon/firstDownload/${fileName}/${token}`;
                    /*gridOptions.api.exportDataAsCsv({
                        fileName: result.value,
                        customHeader: productDescription,
                    });*/
                });
                readAvailableProducts();
                $("#loading").addClass("d-none");
                $("#loading").fadeOut(500);
            },
            500: (response) => {
                $("#loading").addClass("d-none");
                $("#loading").fadeOut(500);
                console.log(response);
            },
        },
    });
}
