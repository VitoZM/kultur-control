var token;

function isLogged(permission) {
    if (sessionStorage.token || localStorage.token) {
        if (sessionStorage.token) token = sessionStorage.token;
        else token = localStorage.token;
        me(token, permission);
    } else window.location = "login";
}

function setAllPermissions(user) {
    user.privileges.forEach((privilege) => {
        let permission = privilege.name;
        $("." + permission).removeClass("d-none");
        $("." + permission).addClass("d-block");
    });
}

function setUserName(user) {
    $(".user-name").html(user.userName);
}

function setJob() {
    let profile = JSON.parse(localStorage.profile);
    $(".role").html(profile.role);
}

function me(token, permission) {
    let url = "api/auth/me";
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        token: token,
        permission: permission,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        //async: false,
        statusCode: {
            200: (response) => {
                user = response;
                setUserName(user);
                setJob();
                setAllPermissions(user);
            },
            400: (response) => {
                console.log(response);
                //window.location = "admin";
            },
            401: (response) => {
                console.log(response);
                localStorage.removeItem("token");
                sessionStorage.removeItem("token");
                //window.location = "login";
            },
            500: (response) => {
                if (response.responseJSON.message == "Server Error")
                    location.reload();
            },
        },
        succes: (result) => {
            //console.log(result);
        },
    });
}

$("#logOutBtn").on("click", (event) => {
    event.preventDefault();
    storageClean();

    let url = "api/log/update";
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        token: token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                //console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    window.location = "login";
});

function storageClean() {
    localStorage.removeItem("token");
    localStorage.removeItem("profile");
    sessionStorage.removeItem("token");
}
