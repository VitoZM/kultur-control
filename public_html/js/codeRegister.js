var actualCodeId;
var validatedToken;
isLogged("create-codes");
isInitiated();
read();

function isInitiated() {
    if (localStorage.response) {
        response = JSON.parse(localStorage.response);
        validatedToken = response.validatedToken;
        fillSuccess(response);
    }
}

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/code/read";
    $.post({
        url: url,
        headers: headers,
        success: (response) => {
            fillCodes(response.codesJson);
        },
    });
}

function showModal(event, codeId) {
    event.preventDefault();
    let url = "api/code/findById";
    let data = { codeId: codeId };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                let code = response.code;
                //console.log(code);
                fillModal(code);
                $("#codeModal").modal("toggle");
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillModal(code) {
    $("#modalCreatedAt").val(parseLaravelDate(code.created_at));
    $("#modalCurrency").val(code.currency);
    $("#modalDeliveredQuantity").val(code.deliveredQuantity);
    $("#modalPaymentId").val(code.paymentId);
    $("#modalProductCode").val(code.productCode);
    $("#modalProductDescription").val(code.productDescription);
    $("#modalQuantity").val(code.quantity);
    $("#modalRecommendedRetailPrice").val(code.recommendedRetailPrice);
    $("#modalReferenceId").val(code.referenceId);
    $("#modalTaxAmount").val(code.taxAmount);
    $("#modalTotalPayablePrice").val(code.totalPayablePrice);
    $("#modalUserId").val(code.userId);
    $("#modalStatus").val(getStringStatus(code.status));
}

function getStringStatus(status) {
    switch (status) {
        case "1":
            return "Disponible";
        case "0":
            return "En Proceso";
        case "2":
            return "Agotado";
    }
}

function parseLaravelDate(date) {
    let dateParts = date.split("T")[0].split("-");
    let time = date.split("T")[1].split(".")[0];

    let day = dateParts[2];
    let month = dateParts[1];
    let year = dateParts[0];

    return `${day}/${month}/${year} ${time}`;
}

function getStringDate(date) {
    let dateParts = date.split("-");
    let day = dateParts[2];
    let month = getStringMonth(dateParts[1]);
    let year = dateParts[0];

    return `${day} ${month}, ${year}`;
}

function getStringMonth(number) {
    let months = [
        "-",
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];
    return months[parseInt(number)];
}

$("#initiateBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    create();
});

$("#updateBtn").on("click", (event) => {
    event.preventDefault();
    if (updateError()) return;
    update();
});

function updateError() {
    let priceError = validateModalPrice();

    if (priceError) return true;
    return false;
}

function error() {
    let quantityError = validateQuantity();
    let quantityError2 = validateQuantity2();
    let productCodeError = validateProductCode();

    if (productCodeError || quantityError || quantityError2) return true;
    return false;
}

function validateQuantity() {
    let quantity = $("#quantity").val().trim();
    if (quantity == "") {
        $("#quantityErrorMessage").removeClass("d-none");
        $("#quantityErrorMessage").addClass("d-block");
        $("#quantity").focus();
        return true;
    } else {
        $("#quantityErrorMessage").removeClass("d-block");
        $("#quantityErrorMessage").addClass("d-none");
        return false;
    }
}

function validateQuantity2() {
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    if (quantity < 1 || quantity > 10) {
        $("#quantityErrorMessage2").removeClass("d-none");
        $("#quantityErrorMessage2").addClass("d-block");
        $("#quantity").focus();
        return true;
    } else {
        $("#quantityErrorMessage2").removeClass("d-block");
        $("#quantityErrorMessage2").addClass("d-none");
        return false;
    }
}

function validateProductCode() {
    let productCode = $("#productCode").val().trim();
    if (productCode == "") {
        $("#productCodeErrorMessage").removeClass("d-none");
        $("#productCodeErrorMessage").addClass("d-block");
        $("#productCode").focus();
        return true;
    } else {
        $("#productCodeErrorMessage").removeClass("d-block");
        $("#productCodeErrorMessage").addClass("d-none");
        return false;
    }
}

function create() {
    $("#loading").removeClass("d-none");
    $("#loading").fadeIn(500);
    let productCode = $("#productCode").val();
    let quantity = $("#quantity").val();
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let data = {
        productCode: productCode,
        quantity: quantity,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/code/create";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                initiateRequest(response.code);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function initiateRequest(data) {
    let url = "https://sandbox-api.mol.com/pinstore/purchaseinitiation";
    //url = "https://api.mol.com/pinstore/purchaseinitiation";
    $.post({
        url: url,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        statusCode: {
            200: (response) => {
                $("#loading").fadeOut(500);
                console.log(response);
                if (response.initiationResultCode == "00") {
                    fillSuccess(response);
                    localStorage.response = JSON.stringify(response);
                    validatedToken = response.validatedToken;
                } else showError(response.initiationResultCode);
            },
            500: (response) => {
                console.log(response);
            },
            400: (response) => {
                console.log(data);
                console.log(response);
            },
            401: (response) => {
                Swal.fire({
                    type: "error",
                    title: "ERROR DE IP",
                    text:
                        "La IP no está registrada, si usted ya registró la ip en la página, debe esperar una hora para que se efectuen los cambios.",
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    deleteActualRequest();
                });
            },
        },
    });
}

$("#purchaseBtn").on("click", function () {
    $("#loading").fadeIn(500);
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let url = "api/code/purchaseRequest";
    let data = {
        validatedToken: validatedToken,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                purchase(response.code);
            },
            500: (response) => {
                console.log(response);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: `Hubó un error en el servidor, intenté nuevamente`,
                    confirmButtonText: `Aceptar`,
                });
                clean();
                read();
            },
        },
    });
});

$("#reTryBtn").on("click", function () {
    $("#loading").fadeIn(500);
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let url = "api/code/reTryPurchaseRequest";
    let data = {
        codeId: codeId,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                purchase(response.code);
            },
            500: (response) => {
                console.log(response);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: `Hubó un error en el servidor, intenté nuevamente`,
                    confirmButtonText: `Aceptar`,
                });
                clean();
                read();
            },
        },
    });
});

function purchase(data) {
    let url = "https://sandbox-api.mol.com/pinstore/purchaseconfirmation";
    //url = "https://api.mol.com/pinstore/purchaseconfirmation";
    $.post({
        url: url,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        statusCode: {
            200: (response) => {
                console.log(response);
                console.log(JSON.stringify(response.coupons));
                if (response.purchaseStatusCode == "00") {
                    response.couponsString = JSON.stringify(response.coupons)
                        .split("\n")
                        .join("");
                    updatePurchasedRequest(response);
                    Swal.fire({
                        type: "success",
                        title: "EXITO",
                        text: `Producto comprado exitosamente`,
                        confirmButtonText: `Aceptar`,
                    });
                    clean();
                    read();
                    $("#loading").fadeOut(500);
                } else showError(response.purchaseStatusCode);
            },
            500: (response) => {
                console.log(response);
            },
            400: (response) => {
                console.log(data);
                console.log(response);
            },
        },
    });
}

function updatePurchasedRequest(data) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let url = "api/code/updatePurchasedRequest";
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillSuccess(response) {
    $(".currency").removeClass("d-none");
    $("#currency").val(response.currency);
    $("#estimateUnitPrice").val(response.estimateUnitPrice);
    $(".estimateUnitPrice").removeClass("d-none");
    $("#referenceId").val(response.referenceId);
    $(".referenceId").removeClass("d-none");
    $("#initiateBtn").addClass("d-none");
    $("#cancelBtn").removeClass("d-none");
    $("#purchaseBtn").removeClass("d-none");
    $("#productCode").attr("readonly", "true");
    $("#quantity").attr("readonly", "true");
}

$("#cancelBtn").on("click", function () {
    $("#loading").fadeIn(500);
    deleteActualRequest();
});

function deleteActualRequest() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let url = "api/code/deleteActualRequest";
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Código eliminado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                });
                clean();
                read();
                $("#loading").fadeOut(500);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function showError(status) {
    let text = "";
    switch (status) {
        case "01":
            text = "El pago no ha sido completado en medio del proceso";
            break;
        case "02":
            text = "Producto Agotado";
            break;
        case "04":
            text = "Saldo Insuficiente Para Procesar";
            break;
        case "05":
            text = "Código de Producto Inválido";
            break;
        case "06":
            text = "Se Está Usando La Misma Referencia de un Producto Anterior";
            break;
        case "07":
            text = "Producto Inválido Para Su País";
            break;
    }
    Swal.fire({
        type: "error",
        title: "ERROR",
        text: text,
        confirmButtonText: `Aceptar`,
    }).then((response) => {
        deleteActualRequest();
    });
}

function clean() {
    $("#productCode").val("");
    $("#quantity").val("");
    $(".currency").addClass("d-none");
    $("#currency").val("");
    $("#estimateUnitPrice").val("");
    $(".estimateUnitPrice").addClass("d-none");
    $("#referenceId").val("");
    $(".referenceId").addClass("d-none");
    $("#initiateBtn").removeClass("d-none");
    $("#cancelBtn").addClass("d-none");
    $("#purchaseBtn").addClass("d-none");
    $("#productCode").removeAttr("readonly");
    $("#quantity").removeAttr("readonly");
    localStorage.removeItem("response");
    validatedToken = "";
}

function getNumericDate(date) {
    let dateParts = date.split(" ");
    let day = dateParts[0];
    let month = getNumericMonth(
        dateParts[1].substr(0, dateParts[1].length - 1)
    );
    let year = dateParts[2];

    return `${year}/${month}/${day}`;
}

function getNumericMonth(month) {
    let months = [
        "-",
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];
    return months.indexOf(month);
}
