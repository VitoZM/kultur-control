var gridOptions;
isLogged("read-coupons");
read();
clearCache();
function clearCache() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/clearCache";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {},
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/readWithdrawals";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                fillWithdrawals(response.withdrawals);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillViewModal(withdrawalId) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/findById";
    let data = { withdrawalId: withdrawalId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                fillModal(response.withdrawnCoupons);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillModal(withdrawnCoupons) {
    let i = 0;
    let html = `<div class="col-sm-6">
                    <fieldset
                        class="
                            form-label-group
                            form-group
                            position-relative
                            has-icon-left
                        "
                    >
                        <input
                            type="text"
                            class="form-control"
                            id="withdrawalIdModal"
                            placeholder="N° Retiro"
                            value="RE-${withdrawnCoupons[0].withdrawalId}"
                            readonly
                        />
                        <div class="form-control-position">
                            <i class="feather icon-user"></i>
                        </div>
                        <label for="withdrawalIdModal">N° Retiro</label>
                    </fieldset>
                </div>
                <div class="col-sm-6">
                    <fieldset
                        class="
                            form-label-group
                            form-group
                            position-relative
                            has-icon-left
                        "
                    >
                        <input
                            type="text"
                            class="form-control"
                            id="productoDescriptionModal"
                            placeholder="Producto"
                            readonly
                            value="${withdrawnCoupons[0].productDescription}"
                        />
                        <div class="form-control-position">
                            <i class="feather icon-lock"></i>
                        </div>
                        <label for="productoDescriptionModal">Producto</label>
                    </fieldset>
                </div>
                <div class="col-sm-6">
                    <fieldset
                        class="
                            form-label-group
                            form-group
                            position-relative
                            has-icon-left
                        "
                    >
                        <input
                            type="text"
                            class="form-control"
                            id="nameModal"
                            placeholder="Nombre"
                            readonly
                            value="${withdrawnCoupons[0].name}"
                        />
                        <div class="form-control-position">
                            <i class="feather icon-lock"></i>
                        </div>
                        <label for="nameModal">Nombre</label>
                    </fieldset>
                </div>
                `;

    withdrawnCoupons.forEach((coupon) => {
        i++;
        html += `<div class="col-md-6">
                        <fieldset
                            class="
                            form-label-group
                            form-group
                            position-relative
                            has-icon-left
                            "
                        >
                            <input type="text" class="form-control"
                                id="pin-${i}"
                                placeholder="Pin ${i}"
                                value="${coupon.pin}"
                                readonly
                            />
                            <div class="form-control-position">
                                <i class="feather icon-user"></i>
                            </div>
                            <label for="pin-${i}">Pin ${i}</label>
                        </fieldset>
                    </div>`;
        $("#modalBody").html(html);
    });

    $("#viewModal").modal("show");
}

function downloadCoupons(withdrawalId) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/withdrawal/updateDownload";
    let data = { withdrawalId: withdrawalId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                console.log(response);
                window.location = `coupon/download/RE-${withdrawalId}/${token}`;
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
