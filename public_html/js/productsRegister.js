isLogged("create-codes");
var newProduct = false;
readProducts();
function readProducts() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/code/getProducts";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#productDescriptions").empty();
                $option = $("<option />", {
                    text: "",
                    value: "",
                    selected: "true",
                });
                $("#productDescriptions").prepend($option);
                let products = response.products;
                products.forEach((product) => {
                    $option = $("<option />", {
                        text: product.productDescription,
                        value: product.productDescription,
                    });
                    $("#productDescriptions").prepend($option);
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#newProductBtn").on("click", function (e) {
    e.preventDefault();
    $("#productDescriptionsDiv").fadeOut();
    $("#productDescriptionDiv").removeClass("d-none");
    $("#productDescriptionDiv").fadeIn();
    $("#oldProductBtnDiv").removeClass("d-none");
    $("#oldProductBtnDiv").fadeIn();
    $("#newProductBtnDiv").fadeOut();
    newProduct = true;
});

$("#oldProductBtn").on("click", function (e) {
    e.preventDefault();
    $("#productDescriptionsDiv").fadeIn();
    $("#productDescriptionDiv").fadeOut();
    $("#oldProductBtnDiv").fadeOut();
    $("#newProductBtnDiv").fadeIn();
    newProduct = false;
});

$("#initiateBtn").on("click", function (e) {
    e.preventDefault();
    let quantityError = validateQuantity();
    if (quantityError) return;
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    addPinInputs(quantity);
});

$("#cancelBtn").on("click", function (e) {
    e.preventDefault();
    $("#inputPins").html("");
    $("#initiateBtn").fadeIn();
    $("#cancelBtn").fadeOut();
    $("#createBtn").fadeOut();
    $("#quantity").removeAttr("readonly");
});

$("#createBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let quantityError = validateQuantity();
    let productDescriptionError;
    if (newProduct) productDescriptionError = validateProductDescription();
    else productDescriptionError = validateProductDescriptions();
    let pinError = validatePin();
    if (productDescriptionError || quantityError || pinError) return true;
    return false;
}

function create() {
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    let productDescription;
    if (newProduct) productDescription = $("#productDescription").val().trim();
    else productDescription = $("#productDescriptions").val().trim();
    let pins = getPins(quantity);
    let data = {
        quantity: quantity,
        productDescription: productDescription,
        pins: pins,
    };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/code/createNewCode";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "ÉXITO",
                    text: `Los pines fueron registrados exitosamente`,
                    confirmButtonText: `Aceptar`,
                });
                clean();
            },
        },
    });
}

function clean() {
    readProducts();
    $("#quantity").removeAttr("readonly");
    $("#quantity").val("");
    $("#inputPins").html("");
    $("#initiateBtn").fadeIn();
    $("#cancelBtn").fadeOut();
    $("#createBtn").fadeOut();
}

function getPins(quantity) {
    let pins = [];
    for (let i = 1; i <= quantity; i++) {
        let pin = $("#pin-" + i)
            .val()
            .trim();
        if (pin != "") pins.push(pin);
    }
    return pins;
}

function validatePin() {
    let quantity = $("#quantity").val().trim();
    quantity = parseInt(quantity);
    for (let i = 1; i <= quantity; i++) {
        let pin = $("#pin-" + i)
            .val()
            .trim();
        if (pin != "") return false;
    }
    Swal.fire({
        type: "error",
        title: "ERROR",
        text: `Debes llenar al menos 1 pin`,
        confirmButtonText: `Aceptar`,
    });
    return true;
}

function validateQuantity() {
    let quantity = $("#quantity").val().trim();
    if (quantity == "") {
        $("#quantityErrorMessage").removeClass("d-none");
        $("#quantityErrorMessage").addClass("d-block");
        $("#quantity").focus();
        return true;
    } else {
        $("#quantityErrorMessage").removeClass("d-block");
        $("#quantityErrorMessage").addClass("d-none");
        return false;
    }
}

function validateProductDescription() {
    let productDescription = $("#productDescription").val().trim();
    if (productDescription == "") {
        $("#productDescriptionErrorMessage").removeClass("d-none");
        $("#productDescriptionErrorMessage").addClass("d-block");
        $("#productDescription").focus();
        return true;
    } else {
        $("#productDescriptionErrorMessage").removeClass("d-block");
        $("#productDescriptionErrorMessage").addClass("d-none");
        return false;
    }
}

function validateProductDescriptions() {
    let productDescriptions = $("#productDescriptions").val().trim();
    if (productDescriptions == "") {
        $("#productDescriptionsErrorMessage").removeClass("d-none");
        $("#productDescriptionsErrorMessage").addClass("d-block");
        $("#productDescriptions").focus();
        return true;
    } else {
        $("#productDescriptionsErrorMessage").removeClass("d-block");
        $("#productDescriptionsErrorMessage").addClass("d-none");
        return false;
    }
}

function addPinInputs(quantity) {
    $("#initiateBtn").fadeOut();
    $("#cancelBtn").removeClass("d-none");
    $("#createBtn").removeClass("d-none");
    $("#cancelBtn").fadeIn();
    $("#createBtn").fadeIn();
    $("#quantity").attr("readonly", "true");
    for (let i = 1; i <= quantity; i++) {
        let html = `<fieldset class="form-label-group form-group position-relative has-icon-left">
                        <input
                            type="text"
                            class="form-control"
                            id="pin-${i}"
                            placeholder="Pin ${i}"
                        />
                        <div class="form-control-position">
                            <i class="feather icon-datetime"></i>
                        </div>
                        <label for="pin-${i}">Pin ${i}</label>
                    </fieldset>`;
        $("#inputPins").append(html);
    }
}

///////////////

var fileobj;
var lector = new FileReader();
var lines;
function upload_file(e) {
    e.preventDefault();
    fileobj = e.dataTransfer.files[0];
    ajax_file_upload(fileobj);
}

function file_explorer() {
    document.getElementById("selectfile").click();
    document.getElementById("selectfile").onchange = function () {
        fileobj = document.getElementById("selectfile").files[0];
        ajax_file_upload(fileobj);
    };
}

function ajax_file_upload(file_obj) {
    if (file_obj != undefined) {
        lector.onload = function (e) {
            var contenido = e.target.result;
            mostrarContenido(file_obj);
        };
        lector.readAsText(file_obj);
        lector.onload = onLoad;
    }
}

function onLoad() {
    var result = lector.result; 
    var lineas = result.split("\n");
    
    lines = [];
    let html = "";
    let i = 0;
    for (var linea of lineas) {
       
        if (linea.length > 1) {
            console.log(linea);
            i++;
            linea = linea.trim();
            html += `<p>${i}. ${linea}</p>`;
            lines.push(linea);
        }
    }
    if (html == "")
        Swal.fire({
            type: "error",
            title: "ERROR",
            text: `El archivo está vacío`,
            confirmButtonText: `Aceptar`,
        });
    else{
        $("#pinsFromText").html(html);
        $("#exampleModal").modal("show");
    }
}

$("#saveMassivePins").on("click", function (e) {
    e.preventDefault();
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/coupon/createMassiveCoupons";
    let productDescription = $("#productMasiveDescription").val().trim();
    if(productDescription.length <= 1){
        Swal.fire({
            type: "error",
            title: "ERROR",
            text: `Debes llenar el nombre del producto`,
            confirmButtonText: `Aceptar`,
        });
        return;
    }
    let data = {productDescription,lines}
    $.post({
        url,
        headers,
        data,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "ÉXITO",
                    text: `Pines registrados exitosamente`,
                    confirmButtonText: `Aceptar`,
                });
                $("#exampleModal").modal("hide");
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
});
