var actualUserId;
isLogged("read-users");
read();

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/read";
    $.post({
        url: url,
        headers: headers,
        success: (response) => {
            fillUsers(response.usersJson);
        },
    });
}

function deleteUser(userId) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/delete";
    let data = {
        userId: userId,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                //console.log(response);
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Usuario eliminado Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {
                    read();
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#deleteBtn").on("click", (event) => {
    event.preventDefault();
    deleteUser(actualUserId);
});

function cleanErrors() {
    $("#nameErrorMessage").removeClass("d-block");
    $("#nameErrorMessage").addClass("d-none");
    $("#lastNameErrorMessage").removeClass("d-block");
    $("#lastNameErrorMessage").addClass("d-none");
}

function fillEditModal(userId, userName) {
    cleanErrors();
    actualUserId = userId;
    $("#formUserName").val(userName);
}

function getStringMonth(number) {
    let months = [
        "-",
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre",
    ];
    return months[parseInt(number)];
}

$("#updateBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    update();
    $("#editarUsuario").modal("hide");
});

function error() {
    let confirmPasswordError = validateConfirmPassword();
    let newPasswordError = validateNewPassword();
    let actualPasswordError = validateActualPassword();

    if (actualPasswordError || newPasswordError || confirmPasswordError)
        return true;
    return false;
}

function validateActualPassword() {
    let actualPassword = $("#actualPassword").val().trim();
    if (actualPassword == "") {
        $("#actualPasswordErrorMessage").removeClass("d-none");
        $("#actualPasswordErrorMessage").addClass("d-block");
        $("#actualPassword").focus();
        return true;
    } else {
        $("#actualPasswordErrorMessage").removeClass("d-block");
        $("#actualPasswordErrorMessage").addClass("d-none");
        return false;
    }
}

function validateNewPassword() {
    let newPassword = $("#newPassword").val().trim();
    if (newPassword == "") {
        $("#newPasswordErrorMessage").removeClass("d-none");
        $("#newPasswordErrorMessage").addClass("d-block");
        $("#newPassword").focus();
        return true;
    } else {
        $("#newPasswordErrorMessage").removeClass("d-block");
        $("#newPasswordErrorMessage").addClass("d-none");
        return false;
    }
}

function validateConfirmPassword() {
    let confirmPassword = $("#confirmPassword").val().trim();
    if (confirmPassword == "") {
        $("#confirmPasswordErrorMessage").removeClass("d-none");
        $("#confirmPasswordErrorMessage").addClass("d-block");
        $("#confirmPassword").focus();
        return true;
    } else {
        $("#confirmPasswordErrorMessage").removeClass("d-block");
        $("#confirmPasswordErrorMessage").addClass("d-none");
        return false;
    }
}

function update() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let actualPassword = $("#actualPassword").val().trim();
    let newPassword = $("#newPassword").val().trim();
    let confirmPassword = $("#confirmPassword").val().trim();

    let url = "api/user/update";
    $.post({
        url: url,
        headers: headers,
        data: {
            userId: actualUserId,
            actualPassword,
            newPassword,
            confirmPassword,
        },
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "EXITO",
                    text: `Contraseña editada Exitosamente`,
                    confirmButtonText: `Aceptar`,
                }).then((response) => {});
            },
            501: (response) => {
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: `Usted no puede editar esta contraseña`,
                    confirmButtonText: `Aceptar`,
                });
            },
            502: (response) => {
                console.log(response.responseJSON);
                Swal.fire({
                    type: "error",
                    title: "ERROR",
                    text: `Las contraseñas no coinciden`,
                    confirmButtonText: `Aceptar`,
                });
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
