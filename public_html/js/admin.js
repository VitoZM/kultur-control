isLogged("user");
read();
readCheckins();
var actualUserName = null;
var actualFullName = null;
function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                fillCisSelect(response.usersJson);
            },
        },
        success: (response) => {},
    });
}

function readCheckins() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/checkin/read2";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                fillCheckins(response.checkins);
            },
        },
        success: (response) => {},
    });
}

function fillCisSelect(users) {
    $option = $("<option />", {
        text: "",
        value: "",
        selected: "true",
    });
    $("#cis").prepend($option);
    users.forEach((user) => {
        $option = $("<option />", {
            text: user.userName,
            value: user.userName,
        });
        $("#cis").prepend($option);
    });
}

$("#cis").on("change", function () {
    let ci = $("#cis").val();
    getProfileAndStatus(ci);
    $("#fullName").addClass("loading-input");
    $("#status").addClass("loading-input");
});

function getProfileAndStatus(userName) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/getProfileAndStatus";
    let data = { userName };
    $.post({
        url,
        headers,
        data,
        statusCode: {
            200: (response) => {
                let { name, lastName, userName } = response.user;
                let status = response.status;
                actualFullName = `${name} ${lastName}`;
                actualUserName = userName;
                $("#fullName").val(actualFullName);
                setStatus(status);
            },
        },
        success: (response) => {
            $("#fullName").removeClass("loading-input");
            $("#status").removeClass("loading-input");
        },
    });
}

function getProfileAndStatus2(userName) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/getProfileAndStatus";
    let data = { userName };
    $.post({
        url,
        headers,
        data,
        statusCode: {
            200: (response) => {
                let { name, lastName, userName } = response.user;
                let status = response.status;
                actualFullName = `${name} ${lastName}`;
                actualUserName = userName;
                $("#fullName").val(actualFullName);
                if (status == "ENTRADA") return;
                setStatus(status);
                Swal.fire({
                    title: `<p>¿Estás seguro que deseas marcar la entrada de
                            <b class="text-primary">${actualFullName}</b> con C.I.: 
                            <b class="text-primary">${actualUserName}</b>?</p>`,
                    showCancelButton: true,
                    confirmButtonText: "MARCAR ENTRADA",
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "green",
                }).then((result) => {
                    if (result.value) {
                        updateStatus();
                    }
                });
            },
        },
        success: (response) => {
            $("#fullName").removeClass("loading-input");
            $("#status").removeClass("loading-input");
        },
    });
}

function setStatus(status) {
    switch (status) {
        case "ENTRADA":
            $("#departureBtn").removeAttr("disabled");
            $("#entryBtn").attr("disabled", "true");
            $("#status").val("MARCÓ ENTRADA");
            break;
        case "SALIDA":
            $("#entryBtn").attr("disabled", "true");
            $("#departureBtn").attr("disabled", "true");
            $("#status").val("MARCÓ SALIDA");
            break;
        default:
            $("#entryBtn").removeAttr("disabled");
            $("#departureBtn").attr("disabled", "true");
            $("#status").val("SIN REGISTRO");
    }
}

$("#entryBtn").on("click", function () {
    Swal.fire({
        title: `<p>¿Estás seguro que deseas marcar la entrada de
                <b class="text-primary">${actualFullName}</b> con C.I.: 
                <b class="text-primary">${actualUserName}</b>?</p>`,
        showCancelButton: true,
        confirmButtonText: "MARCAR ENTRADA",
        cancelButtonText: "Cancelar",
        confirmButtonColor: "green",
    }).then((result) => {
        if (result.value) {
            updateStatus();
        }
    });
});

$("#departureBtn").on("click", function () {
    Swal.fire({
        title: `<p>¿Estás seguro que deseas marcar la salida de
                <b class="text-primary">${actualFullName}</b> con C.I.: 
                <b class="text-primary">${actualUserName}</b>?</p>`,
        showCancelButton: true,
        confirmButtonText: "MARCAR SALIDA",
        cancelButtonText: "Cancelar",
        confirmButtonColor: "red",
    }).then((result) => {
        if (result.value) {
            updateStatus();
        }
    });
});

function rowClick(userNAme) {
    getProfileAndStatus2(userNAme);
}

function updateStatus() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/checkin/updateStatus2";
    let data = { userName: actualUserName };
    $.post({
        url,
        headers,
        data,
        statusCode: {
            200: (response) => {
                let { message, success } = response;
                if (success) {
                    Swal.fire({
                        type: "success",
                        title: "ÉXITO",
                        text: message,
                        confirmButtonText: `Aceptar`,
                    }).then(() => {
                        window.location.reload();
                    });
                } else {
                    Swal.fire({
                        type: "error",
                        title: "ERROR",
                        text: message,
                        confirmButtonText: `Aceptar`,
                    });
                }
            },
        },
        success: (response) => {},
    });
}
